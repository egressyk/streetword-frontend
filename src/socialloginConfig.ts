import {AuthServiceConfig, FacebookLoginProvider} from "angularx-social-login";
import {environment} from "./environments/environment";

export function getAuthServiceConfigs() {
  let config = new AuthServiceConfig([
    {
      id: FacebookLoginProvider.PROVIDER_ID,
      provider: new FacebookLoginProvider(environment.FACEBOOK_APP_ID)
    }
  ]);
  return config;
}
