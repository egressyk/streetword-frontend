import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';

@Injectable()
export class MessagesService {
  messageSelected = new Subject();
  messagePosted = new Subject();
}
