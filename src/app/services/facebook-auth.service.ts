import {AuthService as SocialAuthService, FacebookLoginProvider} from "angularx-social-login";
import {Injectable} from "@angular/core";

@Injectable()
export class FacebookAuthService {
  private socialAuthService;

  constructor(socialAuthService: SocialAuthService) {
    this.socialAuthService = socialAuthService;
  }

  public facebookLogin(): Promise<string> {
    console.log("FB login");
    let socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    return this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        //this will return user data from facebook. What you need is a user token which you will send it to the server
        return userData.authToken;
      }
    );
  }
}
