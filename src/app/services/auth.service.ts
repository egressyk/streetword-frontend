import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {JwtHelperService} from '@auth0/angular-jwt';
import {FacebookAuthService} from './facebook-auth.service';
import {Subscribable} from 'rxjs';
import {StreetwordAPIService} from './streetword-API.service';

const jwt = new JwtHelperService();

@Injectable()
export class AuthService {

  loginObservable: Subscribable<any>;

  private http: HttpClient;
  private streetwordAPIService: StreetwordAPIService;
  private facebookAuthService: FacebookAuthService;
  private urlRoot = environment.STREETWORD_API_URL;

  constructor(http: HttpClient, facebookAuthService: FacebookAuthService, streetwordAPIService: StreetwordAPIService) {
    this.http = http;
    this.facebookAuthService = facebookAuthService;
    this.streetwordAPIService = streetwordAPIService;
  }

  async login(): Promise<any> {
    const fbUserAccessToken = await this.facebookAuthService.facebookLogin();
    let response;
    try {
      response = await this.streetwordAPIService.loginUser(fbUserAccessToken).toPromise();
    } catch (error) {
      console.log('Request error: ', error);
    }
    console.log('Auth service component after login await');
    console.log('Response ', response);
    if (response['token']) {
      this.setToken(response['token']);
      return Promise.resolve();
    }
    return Promise.reject(response);
  }

  async register(username: string): Promise<any> {
    const fbUserAccessToken = await this.facebookAuthService.facebookLogin();
    let response;
    try {
      response = await this.streetwordAPIService.registerUser(username, fbUserAccessToken).toPromise();
      console.log('Request error: ', response);
    } catch (error) {
      console.log('Request error: ', error);
    }
    console.log('Auth service component after register await');
    if (response['token']) {
      this.setToken(response['token']);
      return Promise.resolve();
    }
    return Promise.reject(response);
  }

  logout() {
    localStorage.removeItem(environment.TOKEN_NAME);
  }

  isAuthenticated() {
    return new Promise((resolve, reject) => {
      const token = this.getToken();
      resolve(!jwt.isTokenExpired(token));
    });
  }

  getToken(): string {
    return localStorage.getItem(environment.TOKEN_NAME);
  }

  setToken(token: string) {
    localStorage.setItem(environment.TOKEN_NAME, token);
  }
}
