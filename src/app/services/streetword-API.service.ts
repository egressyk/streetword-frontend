import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {Message} from '../messages/message.model';
import {JwtHelperService} from '@auth0/angular-jwt';

const jwt = new JwtHelperService();

@Injectable()
export class StreetwordAPIService {
  private http: HttpClient;
  private urlRoot = environment.STREETWORD_API_URL;

  constructor(http: HttpClient) {
    this.http = http;
  }

  loginUser(userAccessToken: string) {
    const url = this.urlRoot + "users/login/facebook";
    const body = {"token": userAccessToken};
    return this.http.post(url, body);
  }

  registerUser(username: string, userAccessToken: string) {
    const url = this.urlRoot + 'users/';
    const body = {'username': username, 'token': userAccessToken};
    return this.http.post(url, body);
  }

  postMessage(messageContent: string, position): Observable<any> {
    console.log(Object.keys(position));
    const url = this.urlRoot + 'messages/';
    //const header = this.getHeaderWithToken();
    const body = {
      'author': this.getUserId(),
      'latitude': position.latitude,
      'longitude': position.longitude,
      'content': messageContent
    };
    return this.http.post(url, body);
  }

  getAllMessages(latitude?: string, longitude?: string): Observable<any> {
    const url = this.urlRoot + "messages";
    //const header = this.getHeaderWithToken();
    return this.http.get<Message[]>(url);
  }

  voteMessage(messageId: string, voteValue: boolean): Observable<any> {
    const url = this.urlRoot + 'messages/' + messageId + '/vote';
    //const header = this.getHeaderWithToken();
    const body = {
      'userId': this.getUserId(),
      'vote': voteValue
    };
    return this.http.post(url, body);
  }

  getUserId(): string {
    const token = localStorage.getItem(environment.TOKEN_NAME);
    const decodedToken = jwt.decodeToken(token);
    return decodedToken.id;
  }

}
