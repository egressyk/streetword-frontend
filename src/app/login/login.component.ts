import {Component, OnInit} from '@angular/core';
import {FacebookAuthService} from '../services/facebook-auth.service';
import {AuthService} from '../services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  private facebookAuthService: FacebookAuthService;
  private authService: AuthService;
  private router: Router;
  backendStatus = true;
  isLoading = false;

  constructor(facebookAuthService: FacebookAuthService, authService: AuthService, router: Router) {
    this.authService = authService;
    this.router = router;
    if (this.authService.isAuthenticated()) {
      this.router.navigate([""]);
    }
    this.facebookAuthService = facebookAuthService;
  }

  ngOnInit() {
  }

  async login() {
    this.isLoading = true;
    console.log('Login component login');
    try {
      await this.authService.login();
    } catch (error) {
      console.log(error);
      this.backendStatus = false;
    }
    console.log('Login component after await... should navigate');
    this.router.navigate(['']);
  }

  async register(username: string) {
    console.log(username);
    this.isLoading = true;
    console.log('Login component register');
    try {
      await this.authService.register(username);
    } catch (error) {
      console.log(error);
      this.backendStatus = false;
    }
    console.log('Login component after await... should navigate');
    this.router.navigate(['']);
  }

}
