import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AuthServiceConfig, SocialLoginModule} from 'angularx-social-login';
import {getAuthServiceConfigs} from '../socialloginConfig';

import {AppComponent} from './app.component';
import {LoginComponent} from './login/login.component';
import {RouterModule, Routes} from '@angular/router';
import {AuthService} from './services/auth.service';
import {AuthGuardService} from './services/auth-guard.service';
import {FacebookAuthService} from './services/facebook-auth.service';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {StreetwordAPIService} from './services/streetword-API.service';
import {MessagesMapComponent} from './messages/messages-map/messages-map.component';
import {AgmCoreModule} from '@agm/core';
import {environment} from '../environments/environment';
import {MessagesComponent} from './messages/messages.component';
import {MessagesService} from './services/messages.service';
import {MessageViewComponent} from './messages/message-view/message-view.component';
import {HeaderComponent} from './header/header.component';
import {FooterComponent} from './footer/footer.component';
import {MessageCreateComponent} from './messages/message-create/message-create.component';
import {TokenInterceptor} from './interceptors/token.interceptor';

const appRoutes: Routes = [
  {path: '', canActivate: [AuthGuardService], component: MessagesComponent},
  {path: 'login', component: LoginComponent},
  {path: '**', redirectTo: ''},
];


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MessagesMapComponent,
    MessagesComponent,
    MessageViewComponent,
    HeaderComponent,
    FooterComponent,
    MessageCreateComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    SocialLoginModule,
    RouterModule.forRoot(appRoutes),
    AgmCoreModule.forRoot({
      apiKey: environment.GOOGLE_MAPS_API_KEY
    })
  ],
  providers: [
    {provide: AuthServiceConfig, useFactory: getAuthServiceConfigs},
    AuthService,
    AuthGuardService,
    FacebookAuthService,
    StreetwordAPIService,
    MessagesService,
    {provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
