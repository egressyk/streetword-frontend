import {Component, OnInit} from '@angular/core';
import {StreetwordAPIService} from '../services/streetword-API.service';
import {Message} from './message.model';
import {MessagesService} from '../services/messages.service';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {
  private streetwordAPIService: StreetwordAPIService;
  private messagesService: MessagesService;

  isLoading = false;
  messages: Message[];

  constructor(streetwordAPIService: StreetwordAPIService, messagesService: MessagesService) {
    this.streetwordAPIService = streetwordAPIService;
    this.messagesService = messagesService;
  }

  ngOnInit() {
    this.isLoading = true;
    this.getMessages();
    this.messagesService.messagePosted.subscribe(() => {
      this.getMessages();
    });
  }

  getMessages() {
    this.streetwordAPIService.getAllMessages()
      .subscribe(
        (response) => {
          console.log(response);
          this.messages = response;
          this.isLoading = false;
        },
        (error) => {
          this.isLoading = false;
          console.log('Error - Couldn\'t get messages');
        }
      );
  }
}
