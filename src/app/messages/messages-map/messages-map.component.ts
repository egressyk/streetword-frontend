import {Component, Input, OnInit} from '@angular/core';
import {Message} from '../message.model';
import {MessagesService} from '../../services/messages.service';

//Default coordinates for map centering set to Budapest, Clark Ádám tér
const DEFAULT_LATITUDE = 47.498140;
const DEFAULT_LONGITUDE = 19.040550;

@Component({
  selector: 'app-messages-map',
  templateUrl: './messages-map.component.html',
  styleUrls: ['./messages-map.component.css']
})
export class MessagesMapComponent implements OnInit {
  latitude: number = DEFAULT_LATITUDE;
  longitude: number = DEFAULT_LONGITUDE;

  @Input()messages: Message[];
  messagesService: MessagesService;
  private errorDisplayObject: HTMLElement;

  stylObject = [
    {
      "stylers": [
        {
          "hue": "#ff1a00"
        },
        {
          "invert_lightness": true
        },
        {
          "saturation": -100
        },
        {
          "lightness": 33
        },
        {
          "gamma": 0.5
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#394e6c"
        }
      ]
    }
  ];

  constructor(messagesService: MessagesService) {
    this.messagesService = messagesService;
  }

  ngOnInit() {
    this.errorDisplayObject = document.getElementById("map-error-diplay");
    this.getCurrentLocation();
  }

  getCurrentLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          this.updateLocation(position);
        }, (error) => {
          this.displayError(error);
        });
    }
  }

  updateLocation(position) {
    this.latitude = position.coords.latitude;
    this.longitude = position.coords.longitude;
  }

  displayError(error) {
    switch (error.code) {
      case error.PERMISSION_DENIED:
        this.errorDisplayObject.innerHTML = 'User permission denied';
        break;
      case error.POSITION_UNAVAILABLE:
        this.errorDisplayObject.innerHTML = "Location information unavailable";
        break;
      case error.TIMEOUT:
        this.errorDisplayObject.innerHTML = "Location request timed out";
        break;
      case error.UNKNOWN_ERROR:
        this.errorDisplayObject.innerHTML = "Unknown error";
        break;
    }
    this.errorDisplayObject.style.opacity = "0.8";
    this.errorDisplayObject.style.display = "block";
  }

  onSelectMessage(message: Message) {
    console.log(message);
    this.messagesService.messageSelected.next(message);
  }
}
