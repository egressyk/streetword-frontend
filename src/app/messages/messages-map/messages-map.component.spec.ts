import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MessagesMapComponent} from './messages-map.component';

describe('MessagesMapComponent', () => {
  let component: MessagesMapComponent;
  let fixture: ComponentFixture<MessagesMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MessagesMapComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessagesMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
