import {Component, OnInit} from '@angular/core';
import {StreetwordAPIService} from '../../services/streetword-API.service';
import {MessagesService} from '../../services/messages.service';
import {Message} from '../message.model';

@Component({
  selector: 'app-message-create',
  templateUrl: './message-create.component.html',
  styleUrls: ['./message-create.component.css']
})
export class MessageCreateComponent implements OnInit {
  private streetWordAPIService: StreetwordAPIService;
  private messagesService: MessagesService;
  isComposing = false;

  constructor(streetWordAPIService: StreetwordAPIService, messagesService: MessagesService) {
    this.streetWordAPIService = streetWordAPIService;
    this.messagesService = messagesService;
  }

  ngOnInit() {
  }

  onCompose() {
    this.isComposing = true;
  }

  onCancel() {
    this.isComposing = false;
  }

  onSave(inputField) {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          console.log('Posi' + position.coords);
          this.streetWordAPIService.postMessage(inputField.value, position.coords)
            .subscribe(
              (response: Message) => {
                this.isComposing = false;
                this.messagesService.messagePosted.next();
              }, (error) => {
                console.log(error);
              });

        }, (error) => {
          console.log(error);
          alert('User location access denied');
        });
    }
  }

}
