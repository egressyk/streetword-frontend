export class Message{
  id: string;
  latitude: number;
  longitude: number;
  author: {
    id: string,
    //username: string,
    link: string
  };
  content: string;
  creationDate: Date;
  votes: { string, boolean };
}
