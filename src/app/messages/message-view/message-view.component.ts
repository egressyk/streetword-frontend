import {Component, OnInit} from '@angular/core';
import {Message} from '../message.model';
import {MessagesService} from '../../services/messages.service';
import {Subscription} from 'rxjs';
import {StreetwordAPIService} from '../../services/streetword-API.service';

@Component({
  selector: 'app-message-view',
  templateUrl: './message-view.component.html',
  styleUrls: ['./message-view.component.css']
})
export class MessageViewComponent implements OnInit {
  private messagesService: MessagesService;
  private streetWordAPIService: StreetwordAPIService;
  private messageSelectSubcription: Subscription;
  private messagePostSubscription: Subscription;
  message: Message;
  upVote = 0;
  downVote = 0;

  constructor(messagesService: MessagesService, streetWordAPIService: StreetwordAPIService) {
    this.messagesService = messagesService;
    this.streetWordAPIService = streetWordAPIService;
  }

  ngOnInit() {
    this.messageSelectSubcription = this.messagesService.messageSelected
      .subscribe((message: Message) => {
        this.message = message;
        this.countVotes();
      });
  }

  private countVotes() {
    this.upVote = 0;
    this.downVote = 0;
    if (Object.keys(this.message.votes).length == 0) return;
    for (let vote of Object.values(this.message.votes)) {
      if (vote == true) {
        this.upVote++;
      } else {
        this.downVote++;
      }
    }
  }

  onUpVote() {
    this.streetWordAPIService.voteMessage(this.message.id, true)
      .subscribe(
        (response) => {
          console.log(response);
          this.message = response;
          this.countVotes();
        },
        (error) => {
          console.log('Error - Couldn\'t get message');
        }
      );
  }

  onDownVote() {
    this.streetWordAPIService.voteMessage(this.message.id, false)
      .subscribe(
        (response) => {
          console.log(response);
          this.message = response;
          this.countVotes();
        },
        (error) => {
          console.log('Error - Couldn\'t get message');
        }
      );
  }

}
